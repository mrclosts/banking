# Banking API

This application simulates a banking API. It provides features
for accounts creation and authentication, mail deliveries, banking
operations such as transfer and withdrawal. All of this functionality
accessible via web API.


It's an umbrella project with the following application under it:
`accounts`, `api`, `banking`, `data_vault`, and `mailer`.

### Accounts

As the name says, it manages all the accounts flow in the project.
them.

### API

It's a Phoenix project that handles all requests to the project.
The API is documented on `apps/api/README.md`.

### Banking

It handles the banking operations for the project, and also condenses
reports from transactions performed in the app.

### Data Vault

It manages the Ecto Repo for the project. It's a dependency for all other
applications.

### Mailer

It deals with mail deliveries for the app.


## Installation

Dependecies: Docker and Docker Compose.

```
$ git clone https://mrclosts@bitbucket.org/mrclosts/banking.git
$ cd stone
$ chmod +x run.sh
$ docker-compose up --build
```

The app is now served on `localhost:4000/`.

## Documentation

The `ex_doc` dependency is configured in the application. For generating the
documentation, just run the following command in the umbrella root:

```
$ docker-compose run --rm web mix docs
```

## Testing and Coverage

For running the tests of the application, run the following command:

```
$ docker-compose run --rm web mix test
```

For checking test coverage, just run:

```
$ docker-compose run --rm web mix coveralls -u
```
