# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of the Config module.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
import Config

config :data_vault, DataVault.Repo,
  database: "stone_dev",
  username: System.get_env("DB_USERNAME"),
  password: System.get_env("DB_PASSWORD"),
  hostname: System.get_env("DB_HOSTNAME")

config :data_vault, :ecto_repos, [DataVault.Repo]

config :accounts, Accounts.Guardian,
  issuer: "accounts",
  secret_key: "fKF8w2IhwqFM6D80Kfu0L3Kdv40fl7eWbIbGh/tG4gDzIHwbjqAFwnO59NYqtfE/"

config :accounts, :ecto_repos, [DataVault.Repo]

config :api, :ecto_repos, [DataVault.Repo]

config :banking, :ecto_repos, [DataVault.Repo]
config :banking, :transaction_queue_interval, 1_000 * 60
config :banking, :initial_balance_amount, 1_000

config :mailer, :mail_delivery_interval, 1_000 * 10

# Currency setup (Number)
config :number,
  currency: [unit: "R$", precision: 2, delimiter: ".", separator: ",", format: "%u %n"]

import_config "#{Mix.env()}.exs"
import_config "../apps/api/config/config.exs"
