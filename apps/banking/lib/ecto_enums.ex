import EctoEnum
defenum(BalanceTransactionTypeEnum, :type, [:transfer, :withdrawal])
defenum(BalanceTransactionStatusEnum, :status, [:pending, :finished, :error])
