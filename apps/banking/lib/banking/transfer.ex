defmodule Banking.Transfer do
  @moduledoc """
  The `Banking.Transfer` module contains a `embedded_schema` for the transfer
  operation. It contains only a changeset for validating the external attributes
  from the API.
  """

  use Ecto.Schema

  import Ecto.Changeset

  embedded_schema do
    field(:amount, :decimal)
    field(:destination, :string)
  end

  @required_fields ~w(amount destination)a

  @doc false
  def changeset(transfer, attrs) do
    transfer
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> validate_format(:destination, ~r/@/)
  end
end
