defmodule Banking.TransactionQueue do
  @moduledoc """
  Responsible for scheduling and executing balance  and balance transaction updates.

  It uses the `:queue` module from Erlang for creating the transaction queues and
  also the `GenServer` behaviour for running the updates concurrently.

  This approach is used instead of Redis or RabbitMQ for keeping all the applications
  using only the functionality that Elixir/Erlang provides.
  """

  use GenServer
  require Logger

  @transaction_queue_interval Application.get_env(:banking, :transaction_queue_interval)

  def start_link(_default) do
    GenServer.start_link(__MODULE__, :queue.new(), name: __MODULE__)
  end

  @doc """
  Client API for adding a new item into the queue.
  """
  def add(transaction), do: GenServer.cast(__MODULE__, {:add, transaction})

  @doc """
  Client API for checking if the given transaction is member of the queue.
  """
  def scheduled?(transaction), do: GenServer.call(__MODULE__, {:scheduled?, transaction})

  @impl true
  def init(state) do
    schedule_balance_updates()
    {:ok, state}
  end

  @impl true
  def handle_cast({:add, transaction}, state) do
    {:noreply, :queue.in(transaction, state)}
  end

  @impl true
  def handle_call({:scheduled?, transaction}, _from, state) do
    {:reply, :queue.member(transaction, state), state}
  end

  @impl true
  def handle_info(:process_scheduled_balance_updates, state) do
    log_banking_queue_processing("Start processing banking queue - #{NaiveDateTime.utc_now()}")
    process_scheduled_balance_updates(:queue.out(state))
  end

  @impl true
  def handle_info(_msg, state) do
    {:noreply, state}
  end

  defp schedule_balance_updates do
    Process.send_after(self(), :process_scheduled_balance_updates, @transaction_queue_interval)
  end

  defp process_scheduled_balance_updates({{:value, transaction}, queue}) do
    unless Mix.env() == :test do
      Banking.update_balance_from_transaction(transaction)
    end

    process_scheduled_balance_updates(:queue.out(queue))
  end

  defp process_scheduled_balance_updates({:empty, queue}) do
    log_banking_queue_processing("Start processing banking queue - #{NaiveDateTime.utc_now()}")
    schedule_balance_updates()
    {:noreply, queue}
  end

  defp log_banking_queue_processing(message) do
    if Mix.env() == :prod do
      Logger.info(message)
    end
  end
end
