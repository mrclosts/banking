defmodule Banking.BalanceTransaction do
  @moduledoc """
  The `Banking.BalanceTransaction` module contains the schema for the
  "balance_transactions" table, the changesets for creating and updating a transaction,
  and the queries for the report.
  """

  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Banking.BalanceTransaction
  alias Ecto.Multi

  schema "balance_transactions" do
    field(:amount, :decimal, null: false)
    field(:type, BalanceTransactionTypeEnum, null: false)
    field(:status, BalanceTransactionStatusEnum, null: false)
    field(:operation_code, :string, null: false)
    field(:current_balance_amount, :decimal, virtual: true)
    belongs_to(:balance, Banking.Balance)

    timestamps()
  end

  @doc """
  Creates a changeset for a `%Banking.BalanceTransaction{}` that *adds* money to
  a balance (the "destination transaction" from a transfer operation).
  """
  def increase_balance_changeset(balance_transaction, attrs) do
    balance_transaction
    |> create_changeset(attrs)
    |> add_database_constraints()
  end

  @doc """
  Creates a changeset for a `%Banking.BalanceTransaction{}` that *removes* money from
  a balance (the "source transaction" from a transfer operation and the withdrawal operation).
  """
  def decrease_balance_changeset(balance_transaction, attrs) do
    balance_transaction
    |> create_changeset(attrs)
    |> validate_required([:current_balance_amount])
    |> validate_balance_availability()
    |> update_change(:amount, &Decimal.minus/1)
    |> add_database_constraints()
  end

  defp create_changeset(balance_transaction, attrs) do
    balance_transaction
    |> cast(attrs, [:amount, :type, :operation_code, :current_balance_amount])
    |> validate_required([:type, :operation_code])
    |> put_change(:status, :pending)
  end

  defp validate_balance_availability(%Ecto.Changeset{} = changeset) do
    current_balance_amount = get_change(changeset, :current_balance_amount)

    if current_balance_amount do
      validate_number(changeset, :amount,
        greater_than: 0,
        less_than_or_equal_to: current_balance_amount
      )
    else
      changeset
    end
  end

  defp add_database_constraints(changeset) do
    changeset
    |> foreign_key_constraint(:balance_id)
    |> unique_constraint(:balance_id)
  end

  @doc """
  Associates the given balance to a `%Banking.BalanceTransaction{}`.
  """
  def associate_with_balance(balance, attrs) do
    Ecto.build_assoc(balance, :balance_transactions, attrs)
  end

  @doc """
  Creates a group of database operations using `Ecto.Multi` for updating
  the balances of the transactions of a transfer operation.
  """
  def update_operations_from_transfer(source_transaction, destination_transaction) do
    Multi.append(
      upsert_operation(:source_balance_transaction, source_transaction, %{status: :finished}),
      upsert_operation(:destination_balance_transaction, destination_transaction, %{
        status: :finished
      })
    )
  end

  @doc """
  Creates a group of database operations using `Ecto.Multi` for updating
  the balance of the transaction of a withdrawal operation.
  """
  def update_operation_from_withdrawal(transaction) do
    upsert_operation(:balance_transaction, transaction, %{status: :finished})
  end

  defp upsert_operation(operation_name, balance_transaction, attrs) do
    Multi.new()
    |> Multi.insert_or_update(operation_name, update_status_changeset(balance_transaction, attrs))
  end

  defp update_status_changeset(balance_transaction, attrs) do
    balance_transaction
    |> cast(attrs, [:status])
    |> validate_required([:status])
  end

  @doc """
  Query for finding the transactions for the current day.
  """
  def find_from_period_of_time(:today) do
    from(
      bt in find_transactions_without_error(),
      where: bt.inserted_at > ago(1, "day")
    )
  end

  @doc """
  Query for finding the transactions for the current month.
  """
  def find_from_period_of_time(:current_month) do
    from(
      bt in find_transactions_without_error(),
      where: bt.inserted_at > ago(1, "month")
    )
  end

  @doc """
  Query for finding the transactions for the current year.
  """
  def find_from_period_of_time(:current_year) do
    from(
      bt in find_transactions_without_error(),
      where: bt.inserted_at > ago(1, "year")
    )
  end

  @doc """
  Query for finding the transactions for all time without the status `:error`.
  """
  def find_from_period_of_time(_) do
    find_transactions_without_error()
  end

  defp find_transactions_without_error do
    from(
      bt in BalanceTransaction,
      where: bt.status != "error"
    )
  end

  @doc """
  Query for aggregating the amount of the balance transactions.
  """
  def aggregate_transactions_amount_from_query(period_of_time_query) do
    query = from(bt in period_of_time_query, distinct: bt.operation_code)
    from(bt in subquery(query), select: sum(fragment("abs(?)", bt.amount)))
  end
end
