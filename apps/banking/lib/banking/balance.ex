defmodule Banking.Balance do
  @moduledoc """
  The `Banking.Balance` module contains the schema for the
  "balances" table, the changesets for creating and updating a balance,
  queries for finding a balance from given attributes and `Ecto.Multi` operations.
  """

  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Banking.{BalanceTransaction}
  alias Ecto.Multi

  schema "balances" do
    field(:amount, :decimal, null: false, default: 0)
    field(:account_id, :integer, null: false)

    has_many(:balance_transactions, BalanceTransaction)

    timestamps()
  end

  @required_fields ~w(amount account_id)a

  @doc false
  def changeset(balance, attrs) do
    balance
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> validate_number(:amount, greater_than_or_equal_to: 0)
    |> foreign_key_constraint(:account_id)
    |> unique_constraint(:account_id)
  end

  @doc """
  As we can't use the association utilities from Ecto (eg.: account.balace),
  we need this query to that manually.
  """
  def find_by(%{account_id: account_id}) do
    from(
      balance in Banking.Balance,
      where: balance.account_id == ^account_id
    )
  end

  @doc false
  def find_by(%{account_email: account_email}) do
    from(
      balance in Banking.Balance,
      join: account in "accounts",
      where: account.email == ^account_email,
      where: account.id == balance.account_id
    )
  end

  @doc false
  def find_by(_), do: {:error, :invalid_query_attrs}

  @doc """
  Database operations for updating the balance from a transfer operation.
  """
  def update_operations_from_transfer(source_transaction, destination_transaction) do
    Multi.append(
      update_operation(:source_balance, source_transaction),
      update_operation(:destination_balance, destination_transaction)
    )
  end

  @doc """
  Database operation for updating the balance from a withdrawal operation.
  """
  def update_operation_from_withdrawal(transaction) do
    update_operation(:balance, transaction)
  end

  defp update_operation(operation_name, transaction) do
    Multi.new()
    |> Multi.update(
      operation_name,
      changeset(transaction.balance, %{
        amount:
          Decimal.add(
            transaction.balance.amount,
            transaction.amount
          )
      })
    )
  end
end
