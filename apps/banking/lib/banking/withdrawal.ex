defmodule Banking.Withdrawal do
  @moduledoc """
  The `Banking.Withdrawal` module contains a `embedded_schema` for the withdrawal
  operation. It contains only a changeset for validating the external attributes
  from the API.
  """

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field(:amount, :decimal)
  end

  @doc false
  def changeset(withdrawal, params) do
    withdrawal
    |> cast(params, [:amount])
    |> validate_required(:amount)
  end
end
