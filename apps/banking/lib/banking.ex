defmodule Banking do
  @moduledoc """
  `Banking` is the Public API for the banking application.

  This application is responsible for managing all the banking operations
  available in the project. We currently have two of them: `Banking.Withdrawal` and
  `Banking.Transfer`.

  All of them work the same way. Each one has an `embedded_schema` in
  `lib/banking`, which is responsible for validating the data from
  an external request.

  After that, a new `Banking.BalanceTransaction` is created with all the information
  from the banking operation. This transaction is associated with a `Balance`,
  and scheduled to be processed in predefined intervals in the `Banking.TransactionQueue`
  module. When processed, the associated balance and the transaction will be updated on
  database.

  **Important:** Two transactions will be created for each transfer. One for the
  source balance and other for the destination balance. Both of them will have
  the same `operation_code` attribute.

  A transaction with the type `:withdrawal` will have a negative amount, and
  its value will be subtracted from its associated balance amount.

  An operation with the type `:transfer` will generate two transactions with the
  same token - one with negative amount and another with a positive one. If one
  update fails, the other will too.
  """

  alias Banking.{Balance, BalanceTransaction, Transfer, Withdrawal}
  alias DataVault.Repo
  alias Ecto.{Changeset, Multi}

  @initial_balance_amount Application.get_env(:banking, :initial_balance_amount)

  @doc """
  Transfers money from a source to a destination account.

  Returns `{:ok, {%Banking.BalanceTransaction{type: "transfer", ...}, %Banking.BalanceTransaction{type: "transfer", ...}}}`
  for successful operations.

  Returns `{:error, %Ecto.Changeset{valid?: false}}` for transactions with invalid
  attributes.

  Returns `{:error, :balance_not_found}` for transactions with invalid source or
  destination balance.

  ## Parameters

    - source_account: An `%Accounts.Account{}` struct representing the source account of
    the transfer (where the money comes from)
    - attributes: Map containing the attributes of the banking transference
    (where the money goes to)

  """
  def transfer_money(source_account, attrs \\ %{}) do
    with %Changeset{valid?: true, changes: transfer_attrs} <-
           Transfer.changeset(%Transfer{}, attrs),
         %Balance{} = source_balance <-
           get_balance_from_account_by(%{account_email: source_account.email}),
         %Balance{} = destination_balance <-
           get_balance_from_account_by(%{account_email: transfer_attrs.destination}),
         {:ok, {source_transaction, destination_transaction}} <-
           create_transfer_transactions(source_balance, destination_balance, transfer_attrs) do
      {:ok, {source_transaction, destination_transaction}}
    else
      %Ecto.Changeset{valid?: false} = changeset ->
        {:error, changeset}

      {:error, %Ecto.Changeset{valid?: false} = changeset} ->
        {:error, changeset}

      nil ->
        {:error, :balance_not_found}
    end
  end

  @doc """
  Get money from an account.

  Returns `{:ok, %Banking.BalanceTransaction{type: "withdrawal", ...}}`.

  Returns `{:error, %Ecto.Changeset{valid?: false}}` for transactions with invalid
  attributes.

  Returns `{:error, :balance_not_found}` for transactions with invalid balance.

  ## Parameters

    - account: An `%Accounts.Account{}` struct representing the source account of
    the transfer (where the money comes from)
    - attrs: Map containing the attributes of the banking withdrawal.

  """
  def withdrawal_money(account, attrs \\ %{}) do
    with %Changeset{valid?: true, changes: withdrawal_attrs} <-
           Withdrawal.changeset(%Withdrawal{}, attrs),
         %Balance{} = balance <- get_balance_from_account_by(%{account_email: account.email}),
         {:ok, withdrawal_transaction} <- create_withdrawal_transaction(balance, withdrawal_attrs) do
      {:ok, withdrawal_transaction}
    else
      %Ecto.Changeset{valid?: false} = changeset ->
        {:error, changeset}

      {:error, %Ecto.Changeset{valid?: false} = changeset} ->
        {:error, changeset}

      nil ->
        {:error, :balance_not_found}
    end
  end

  defp create_transfer_transactions(source_balance, destination_balance, transfer_attrs) do
    transfer_attrs =
      transfer_attrs
      |> add_operation_code_to_transaction(Ecto.UUID.generate())
      |> add_type_to_transaction("transfer")
      |> add_current_balance_amount_to_transaction(source_balance.amount)

    with %Changeset{valid?: true, changes: source_changes} <-
           BalanceTransaction.decrease_balance_changeset(%BalanceTransaction{}, transfer_attrs),
         %Changeset{valid?: true, changes: destination_changes} <-
           BalanceTransaction.increase_balance_changeset(%BalanceTransaction{}, transfer_attrs),
         %BalanceTransaction{} = source_transaction <-
           BalanceTransaction.associate_with_balance(source_balance, source_changes),
         %BalanceTransaction{} = destination_transaction <-
           BalanceTransaction.associate_with_balance(destination_balance, destination_changes) do
      {:ok, {source_transaction, destination_transaction}}
    else
      %Ecto.Changeset{valid?: false} = changeset ->
        {:error, changeset}
    end
  end

  defp create_withdrawal_transaction(balance, withdrawal_attrs) do
    withdrawal_attrs =
      withdrawal_attrs
      |> add_operation_code_to_transaction(Ecto.UUID.generate())
      |> add_type_to_transaction("withdrawal")
      |> add_current_balance_amount_to_transaction(balance.amount)

    with %Changeset{valid?: true, changes: transaction_changes} <-
           BalanceTransaction.decrease_balance_changeset(%BalanceTransaction{}, withdrawal_attrs),
         %BalanceTransaction{} = withdrawal_transaction <-
           BalanceTransaction.associate_with_balance(balance, transaction_changes) do
      {:ok, withdrawal_transaction}
    else
      %Ecto.Changeset{valid?: false} = changeset ->
        {:error, changeset}
    end
  end

  defp add_operation_code_to_transaction(transaction_attrs, operation_code) do
    Map.put(transaction_attrs, :operation_code, operation_code)
  end

  defp add_type_to_transaction(transaction_attrs, type) do
    Map.put(transaction_attrs, :type, type)
  end

  defp add_current_balance_amount_to_transaction(transaction_attrs, amount) do
    Map.put(transaction_attrs, :current_balance_amount, amount)
  end

  @doc """
  Schedules a banking balance transaction to be processed in predefined intervals.

  It adds the given `%Banking.BalanceTransaction{}` to `%Banking.TransactionQueue{}`.

  Returns `{:ok, :balance_update_scheduled}`.

  ## Parameters

    - transactions_from_operation: It can be a single transaction (for a withdrawal),
    or a tuple with two transactions (for a transfer).

  """
  def schedule_balance_update(transactions_from_operation) do
    :ok = Banking.TransactionQueue.add(transactions_from_operation)
    {:ok, :balance_update_scheduled}
  end

  @doc """
  Checks if a given `%Banking.BalanceTransaction{}` is scheduled in the queue.
  """
  def transaction_scheduled?(transaction) do
    Banking.TransactionQueue.scheduled?(transaction)
  end

  @doc """
  Creates a new `%Banking.Balance{}` for a given account id.

  Returns `{:ok, %Banking.Balance{}` for successful creations.

  Returns `{:error, %Ecto.Changeset{valid?: false}}` when errors happen.
  """
  def create_balance_for_account(account_id) do
    changeset =
      Balance.changeset(%Balance{}, %{account_id: account_id, amount: @initial_balance_amount})

    case changeset do
      %Changeset{valid?: true} ->
        Repo.insert(changeset)

      _ ->
        {:error, changeset}
    end
  end

  @doc """
  Finds a `%Banking.Balance{}` from a given parameter.

  As we have an umbrella app, and the `%Accounts.Account{}` stays on a different app,
  we need to use ecto queries for getting the associated balance account.

  Returns `%Banking.Balance{}` when the query attributes are valid.

  Returns `{:error, :invalid_query_attrs}` when the query attributes are invalid.

  Returns `nil` when there's no `%Banking.Balance{}` from the account attributes.
  """
  def get_balance_from_account_by(attrs) do
    case query = Balance.find_by(attrs) do
      {:error, :invalid_query_attrs} ->
        {:error, :invalid_query_attrs}

      _ ->
        Repo.one(query)
    end
  end

  @doc """
  Updates the balances from balance transactions of a transfer operation.

  It groups a bunch of database operations using the `Ecto.Multi` module, as we
  MUST execute all changes in a single database transaction for preventing
  inconsistencies.

  Returns `{:ok, :balance_updated}` for a successful operation.

  Returns `{:error, :balance_update_error}` for an error operation. In this
  case, the transaction is re-scheduled into the queue.
  """
  def update_balance_from_transaction(
        {source_transaction, destination_transaction} = balance_transactions
      ) do
    source_transaction = Repo.preload(source_transaction, [:balance])
    destination_transaction = Repo.preload(destination_transaction, [:balance])

    update_balance_operations =
      Balance.update_operations_from_transfer(source_transaction, destination_transaction)

    update_balance_transaction_operations =
      BalanceTransaction.update_operations_from_transfer(
        source_transaction,
        destination_transaction
      )

    update_operations =
      Multi.append(update_balance_operations, update_balance_transaction_operations)

    case do_update_balance_from_operations(update_operations) do
      {:ok, _} ->
        {:ok, :balance_updated}

      {:error, _} ->
        schedule_balance_update(balance_transactions)
        {:error, :balance_update_error}
    end
  end

  @doc """
  Updates the balance from a balance transaction of a withdrawal operation.

  It groups a bunch of database operations using the `Ecto.Multi` module, as we
  MUST execute all changes in a single database transaction for preventing
  inconsistencies.

  Returns `{:ok, :balance_updated}` for a successful operation.

  Returns `{:error, :balance_update_error}` for an error operation. In this
  case, the transaction is re-scheduled into the queue.
  """
  def update_balance_from_transaction(balance_transaction) do
    balance_transaction = Repo.preload(balance_transaction, [:balance])

    update_balance_operation = Balance.update_operation_from_withdrawal(balance_transaction)

    update_balance_transaction_operation =
      BalanceTransaction.update_operation_from_withdrawal(balance_transaction)

    update_operations =
      Multi.append(update_balance_operation, update_balance_transaction_operation)

    case do_update_balance_from_operations(update_operations) do
      {:ok, _} ->
        {:ok, :balance_updated}

      {:error, _} ->
        schedule_balance_update(balance_transaction)
        {:error, :balance_update_error}
    end
  end

  defp do_update_balance_from_operations(update_operations) do
    case Repo.transaction(update_operations) do
      {:ok, result} ->
        {:ok, result}

      {:error, failed_operation, _, _} ->
        {:error, failed_operation}
    end
  end

  @doc """
  It generates a report aggregating the total amount of balance transactions
  from the current day, month, year and total.
  """
  def generate_report do
    %{
      today: aggregate_transaction_amount_from_period_of_time(:today),
      current_month: aggregate_transaction_amount_from_period_of_time(:current_month),
      current_year: aggregate_transaction_amount_from_period_of_time(:current_year),
      total: aggregate_transaction_amount_from_period_of_time(:total)
    }
  end

  defp aggregate_transaction_amount_from_period_of_time(period_of_time) do
    query = BalanceTransaction.find_from_period_of_time(period_of_time)
    query = BalanceTransaction.aggregate_transactions_amount_from_query(query)
    Repo.one(query)
  end
end
