defmodule BankingTest do
  use DataVault.RepoCase, async: true

  import Accounts.Factory

  alias DataVault.Repo

  setup do
    %{account: insert!(:valid_account)}
  end

  describe "create_balance_for_account/1" do
    test "create balance if the account exists", %{account: account} do
      assert {:ok, balance} = Banking.create_balance_for_account(account.id)
      assert Decimal.eq?(balance.amount, 1_000)
    end

    test "return error changeset if the account doesn't exist" do
      assert {:error, changeset} = Banking.create_balance_for_account(42)

      assert %{account_id: ["does not exist"]} = errors_on(changeset)
    end
  end

  describe "get_balance_from_account_by/1" do
    test "return balance if account exists", %{account: account} do
      {:ok, _} = Banking.create_balance_for_account(account.id)

      assert %Banking.Balance{} = Banking.get_balance_from_account_by(%{account_id: account.id})
    end

    test "return error with invalid query attrs " do
      assert {:error, :invalid_query_attrs} = Banking.get_balance_from_account_by(42)
    end
  end

  describe "transfer_money/2" do
    @valid_attrs %{amount: 200, destination: "charlie@mail.com"}
    @invalid_attrs %{amount: nil, destination: nil}

    test "return tuple with source and destination transactions with valid attrs", %{
      account: source_account
    } do
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      {:ok, source_balance} = Banking.create_balance_for_account(source_account.id)
      {:ok, destination_balance} = Banking.create_balance_for_account(destination_account.id)

      assert {:ok, {source_transaction, destination_transaction}} =
               Banking.transfer_money(source_account, @valid_attrs)

      assert source_transaction.balance_id == source_balance.id
      assert destination_transaction.balance_id == destination_balance.id
      assert Decimal.eq?(source_transaction.amount, -200)
      assert Decimal.eq?(destination_transaction.amount, 200)
      assert source_transaction.operation_code == destination_transaction.operation_code
      assert source_transaction.type == :transfer
      assert destination_transaction.type == :transfer
      assert source_transaction.status == :pending
      assert destination_transaction.status == :pending
    end

    test "return error changeset if the attrs are invalid", %{account: source_account} do
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      Banking.create_balance_for_account(source_account.id)
      Banking.create_balance_for_account(destination_account.id)

      assert {:error, %Ecto.Changeset{valid?: false}} =
               Banking.transfer_money(source_account, @invalid_attrs)
    end

    test "return error :balance_not_found if there's no destination account", %{
      account: source_account
    } do
      Banking.create_balance_for_account(source_account.id)
      assert {:error, :balance_not_found} = Banking.transfer_money(source_account, @valid_attrs)
    end

    test "return error :balance_not_found if there's no source balance account", %{
      account: source_account
    } do
      assert {:error, :balance_not_found} = Banking.transfer_money(source_account, @valid_attrs)
    end
  end

  describe "withdrawal_money/2" do
    @valid_attrs %{amount: 200}
    @invalid_attrs %{amount: nil}

    test "return withdrawal transaction if the attrs are valid", %{account: account} do
      {:ok, balance} = Banking.create_balance_for_account(account.id)

      assert {:ok, withdrawal_transaction} = Banking.withdrawal_money(account, @valid_attrs)
      assert withdrawal_transaction.balance_id == balance.id
      assert Decimal.eq?(withdrawal_transaction.amount, -200)
      assert withdrawal_transaction.operation_code
      assert withdrawal_transaction.type == :withdrawal
      assert withdrawal_transaction.status == :pending
    end

    test "return error changeset if the attrs are invalid", %{account: account} do
      Banking.create_balance_for_account(account.id)

      assert {:error, %Ecto.Changeset{valid?: false}} =
               Banking.withdrawal_money(account, @invalid_attrs)
    end

    test "return error :balance_not_found if there's no balance for given account", %{
      account: account
    } do
      assert {:error, :balance_not_found} = Banking.withdrawal_money(account, @valid_attrs)
    end
  end

  describe "schedule_balance_update/1" do
    test "schedule balance update for given transaction" do
      assert {:ok, :balance_update_scheduled} =
               Banking.schedule_balance_update(%Banking.BalanceTransaction{})
    end
  end

  describe "transaction_scheduled?/1" do
    test "return true for scheduled transaction", %{account: account} do
      Banking.create_balance_for_account(account.id)
      assert {:ok, withdrawal_transaction} = Banking.withdrawal_money(account, %{amount: 200})

      assert {:ok, :balance_update_scheduled} =
               Banking.schedule_balance_update(withdrawal_transaction)

      assert Banking.transaction_scheduled?(withdrawal_transaction)
    end

    test "return false for unscheduled transaction", %{account: account} do
      Banking.create_balance_for_account(account.id)
      assert {:ok, withdrawal_transaction} = Banking.withdrawal_money(account, %{amount: 200})
      refute Banking.transaction_scheduled?(withdrawal_transaction)
    end
  end

  describe "update_balance_from_transaction/1" do
    test "update balance amount from transfer if the balance transaction attrs are valid", %{
      account: source_account
    } do
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      Banking.create_balance_for_account(source_account.id)
      Banking.create_balance_for_account(destination_account.id)

      assert {:ok, {source_transaction, destination_transaction}} =
               Banking.transfer_money(source_account, %{
                 amount: 200,
                 destination: "charlie@mail.com"
               })

      assert {:ok, :balance_updated} =
               Banking.update_balance_from_transaction(
                 {source_transaction, destination_transaction}
               )

      source_balance = Banking.get_balance_from_account_by(%{account_id: source_account.id})

      destination_balance =
        Banking.get_balance_from_account_by(%{account_id: destination_account.id})

      assert Decimal.eq?(source_balance.amount, 800)
      assert Decimal.eq?(destination_balance.amount, 1_200)
    end

    test "return update balance error from transfer if the source balance hasn't available amount for completing the operation and then schedule the transaction again",
         %{account: source_account} do
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      {:ok, source_balance} = Banking.create_balance_for_account(source_account.id)
      Banking.create_balance_for_account(destination_account.id)

      assert {:ok, {source_transaction, destination_transaction}} =
               Banking.transfer_money(source_account, %{
                 amount: 200,
                 destination: "charlie@mail.com"
               })

      Repo.update!(Ecto.Changeset.change(source_balance, %{amount: 50}))

      assert {:error, :balance_update_error} =
               Banking.update_balance_from_transaction(
                 {source_transaction, destination_transaction}
               )

      source_balance = Banking.get_balance_from_account_by(%{account_id: source_account.id})

      destination_balance =
        Banking.get_balance_from_account_by(%{account_id: destination_account.id})

      assert Decimal.eq?(source_balance.amount, 50)
      assert Decimal.eq?(destination_balance.amount, 1_000)
      assert Banking.transaction_scheduled?({source_transaction, destination_transaction})
    end

    test "update balance amount from withdrawal if the balance transaction attrs are valid", %{
      account: account
    } do
      Banking.create_balance_for_account(account.id)

      assert {:ok, balance_transaction} = Banking.withdrawal_money(account, %{amount: 200})

      assert {:ok, :balance_updated} =
               Banking.update_balance_from_transaction(balance_transaction)

      balance = Banking.get_balance_from_account_by(%{account_id: account.id})

      assert Decimal.eq?(balance.amount, 800)
    end

    test "return update balance error from withdrawal if the account balance hasn't available amount for completing the operation and then schedule the transaction again",
         %{account: account} do
      {:ok, balance} = Banking.create_balance_for_account(account.id)

      assert {:ok, balance_transaction} = Banking.withdrawal_money(account, %{amount: 200})

      Repo.update!(Ecto.Changeset.change(balance, %{amount: 50}))

      assert {:error, :balance_update_error} =
               Banking.update_balance_from_transaction(balance_transaction)

      balance = Banking.get_balance_from_account_by(%{account_id: account.id})

      assert Decimal.eq?(balance.amount, 50)
    end
  end

  describe "generate_report/0" do
    test "aggregates the absolute amount value of transactions from day, month, year and total",
         %{account: account} do
      today = truncate_naive_date_time(NaiveDateTime.utc_now())
      {:ok, same_month} = NaiveDateTime.new(today.year, today.month, 1, 0, 0, 0)
      same_month = truncate_naive_date_time(same_month)
      {:ok, same_year} = NaiveDateTime.new(today.year, 1, 1, 0, 0, 0)
      same_year = truncate_naive_date_time(same_year)
      {:ok, a_year_ago} = NaiveDateTime.new(today.year - 1, today.month, 1, 0, 0, 0)
      a_year_ago = truncate_naive_date_time(a_year_ago)

      {:ok, balance} = Banking.create_balance_for_account(account.id)

      Repo.insert_all(
        Banking.BalanceTransaction,
        [
          %{
            amount: -100,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: today,
            updated_at: today
          },
          %{
            amount: 300,
            type: :transfer,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: same_month,
            updated_at: today
          },
          %{
            amount: -100,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: same_month,
            updated_at: today
          },
          %{
            amount: -100,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: same_year,
            updated_at: today
          },
          %{
            amount: -200,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: a_year_ago,
            updated_at: today
          }
        ]
      )

      result = Banking.generate_report()

      assert is_map(result)
      assert Decimal.eq?(result.today, 100)
      assert Decimal.eq?(result.current_month, 500)
      assert Decimal.eq?(result.current_year, 600)
      assert Decimal.eq?(result.total, 800)
    end

    test "generate_report/0 considers only one transaction from transfer", %{account: account} do
      operation_code = Ecto.UUID.generate()
      {:ok, balance} = Banking.create_balance_for_account(account.id)
      today = truncate_naive_date_time(NaiveDateTime.utc_now())

      Repo.insert_all(
        Banking.BalanceTransaction,
        [
          %{
            amount: -300,
            type: :transfer,
            status: :pending,
            operation_code: operation_code,
            balance_id: balance.id,
            inserted_at: today,
            updated_at: today
          },
          %{
            amount: 300,
            type: :transfer,
            status: :pending,
            operation_code: operation_code,
            balance_id: balance.id,
            inserted_at: today,
            updated_at: today
          }
        ]
      )

      result = Banking.generate_report()
      assert is_map(result)
      assert Decimal.eq?(result.today, 300)
      assert Decimal.eq?(result.current_month, 300)
      assert Decimal.eq?(result.current_year, 300)
      assert Decimal.eq?(result.total, 300)
    end
  end
end
