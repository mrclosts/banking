defmodule Banking.WithdrawalTest do
  use ExUnit.Case

  import DataVault.RepoCase, only: [errors_on: 1]

  alias Banking.Withdrawal
  alias Ecto.Changeset

  describe "changeset/2" do
    @valid_attrs %{amount: 1_000}
    @invalid_attrs %{amount: nil}

    test "return valid changeset with valid attributes" do
      changeset = Withdrawal.changeset(%Withdrawal{}, @valid_attrs)
      transfer = Changeset.apply_changes(changeset)

      assert changeset.valid?
      assert Decimal.eq?(transfer.amount, 1_000)
    end

    test "return invalid changeset with invalid attributes" do
      changeset = Withdrawal.changeset(%Withdrawal{}, @invalid_attrs)

      refute changeset.valid?

      assert %{amount: ["can't be blank"]} == errors_on(changeset)
    end
  end
end
