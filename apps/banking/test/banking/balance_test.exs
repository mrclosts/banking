defmodule Banking.BalanceTest do
  use DataVault.RepoCase, async: true

  import Accounts.Factory

  alias Banking.Balance
  alias DataVault.Repo

  describe "changeset/2" do
    @valid_attrs %{amount: 1_000, account_id: 1}
    @invalid_attrs %{amount: nil, account_id: nil}

    test "return valid changeset with valid attributes" do
      changeset = Balance.changeset(%Balance{}, @valid_attrs)
      balance = Ecto.Changeset.apply_changes(changeset)

      assert changeset.valid?
      assert Decimal.eq?(balance.amount, 1_000)
      assert balance.account_id == 1
    end

    test "return invalid changeset with invalid attributes" do
      changeset = Balance.changeset(%Balance{}, @invalid_attrs)

      refute changeset.valid?

      assert %{
               amount: ["can't be blank"],
               account_id: ["can't be blank"]
             } == errors_on(changeset)
    end
  end

  describe "find_by/1" do
    test "return Ecto Query if account id is valid" do
      assert %Ecto.Query{} = Balance.find_by(%{account_id: 42})
    end

    test "return Ecto Query if account email is valid" do
      assert %Ecto.Query{} = Balance.find_by(%{account_email: "john@mail.com"})
    end

    test "return error if query for searching balance is invalid" do
      assert {:error, :invalid_query_attrs} = Balance.find_by("account_id")
    end
  end

  describe "update_operations_from_transfer/2" do
    @valid_attrs %{
      amount: 1_000,
      type: :transfer,
      operation_code: "operation-12345",
      current_balance_amount: 2_000
    }

    test "return group of operations with valid changesets if attrs are valid" do
      source_account = insert!(:valid_account, email: "john@mail.com")
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      {:ok, source_balance} = Banking.create_balance_for_account(source_account.id)
      {:ok, destination_balance} = Banking.create_balance_for_account(destination_account.id)

      source = build_assoc(source_balance, :balance_transactions, @valid_attrs)
      destination = build_assoc(destination_balance, :balance_transactions, @valid_attrs)

      source = Repo.preload(source, [:balance])
      destination = Repo.preload(destination, [:balance])

      operations = Balance.update_operations_from_transfer(source, destination)

      assert [
               source_balance: {:update, source_balance_changeset, []},
               destination_balance: {:update, destination_balance_changeset, []}
             ] = Ecto.Multi.to_list(operations)

      assert source_balance_changeset.valid?
      assert destination_balance_changeset.valid?
    end
  end

  describe "update_operations_from_withdrawal/1" do
    @valid_attrs %{
      amount: 1_000,
      type: :withdrawal,
      operation_code: "operation-12345",
      current_balance_amount: 2_000
    }

    test "return group of operations for updating balance and balance transactions" do
      source_account = insert!(:valid_account, email: "john@mail.com")
      {:ok, source_balance} = Banking.create_balance_for_account(source_account.id)

      source = build_assoc(source_balance, :balance_transactions, @valid_attrs)
      source = Repo.preload(source, [:balance])

      operations = Balance.update_operation_from_withdrawal(source)

      assert [balance: {:update, balance_changeset, []}] = Ecto.Multi.to_list(operations)

      assert balance_changeset.valid?
    end
  end
end
