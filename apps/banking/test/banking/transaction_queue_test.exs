defmodule Banking.TransactionQueueTest do
  use DataVault.RepoCase

  alias Banking.{BalanceTransaction, TransactionQueue}

  test "add/1 adds BalanceTransaction to queue" do
    assert :ok = TransactionQueue.add(%BalanceTransaction{})
  end

  test "transaction_scheduled?/1 checks if transaction is scheduled to be processed" do
    transaction = %BalanceTransaction{}
    TransactionQueue.add(transaction)

    assert TransactionQueue.scheduled?(transaction)
  end

  test "receives :process_scheduled_balance_updates message for processing the queue and empty it" do
    assert :ok = TransactionQueue.add(%BalanceTransaction{})

    children = Supervisor.which_children(Banking.Supervisor)

    assert {TransactionQueue, pid, _, _} = List.keyfind(children, TransactionQueue, 0)

    :erlang.trace(pid, true, [:receive])
    assert_receive {:trace, ^pid, :receive, :process_scheduled_balance_updates}, 1_000
  end
end
