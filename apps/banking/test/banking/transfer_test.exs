defmodule Banking.TransferTest do
  use ExUnit.Case

  import DataVault.RepoCase, only: [errors_on: 1]

  alias Banking.Transfer
  alias Ecto.Changeset

  describe "changeset/2" do
    @valid_attrs %{amount: 1_000, destination: "charlie@mail.com"}
    @invalid_attrs %{amount: nil, destination: nil}

    test "return valid changeset with valid attributes" do
      changeset = Transfer.changeset(%Transfer{}, @valid_attrs)
      transfer = Changeset.apply_changes(changeset)

      assert changeset.valid?
      assert transfer.destination == "charlie@mail.com"
      assert Decimal.eq?(transfer.amount, 1_000)
    end

    test "return invalid changeset with invalid attributes" do
      changeset = Transfer.changeset(%Transfer{}, @invalid_attrs)

      refute changeset.valid?

      assert %{
               amount: ["can't be blank"],
               destination: ["can't be blank"]
             } == errors_on(changeset)
    end

    test "return invalid changeset with invalid destination email" do
      changeset = Transfer.changeset(%Transfer{}, %{@valid_attrs | destination: "john"})

      refute changeset.valid?
      assert %{destination: ["has invalid format"]} = errors_on(changeset)
    end
  end
end
