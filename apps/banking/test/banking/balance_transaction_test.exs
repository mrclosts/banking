defmodule Banking.BalanceTransactionTest do
  use DataVault.RepoCase, async: true

  alias Banking.BalanceTransaction
  alias Ecto.Changeset

  describe "increase_balance_changeset/2" do
    @valid_attrs %{amount: 1_000, type: :transfer, operation_code: "operation-12345"}
    @invalid_attrs %{amount: nil, type: nil, operation_code: nil}

    test "return valid changeset with valid attributes" do
      changeset =
        BalanceTransaction.increase_balance_changeset(%BalanceTransaction{}, @valid_attrs)

      balance_transaction = Changeset.apply_changes(changeset)

      assert changeset.valid?
      assert balance_transaction.type == :transfer
      assert balance_transaction.status == :pending
      assert balance_transaction.operation_code == "operation-12345"
      assert Decimal.eq?(balance_transaction.amount, 1_000)
    end

    test "return invalid changeset with invalid attributes" do
      changeset =
        BalanceTransaction.increase_balance_changeset(%BalanceTransaction{}, @invalid_attrs)

      refute changeset.valid?

      assert %{
               type: ["can't be blank"],
               operation_code: ["can't be blank"]
             } == errors_on(changeset)
    end
  end

  describe "decrease_balance_changeset/2" do
    @valid_attrs %{
      amount: 200,
      type: :transfer,
      operation_code: "operation-12345",
      current_balance_amount: 2_000
    }
    @invalid_attrs %{amount: nil, type: nil, operation_code: nil, current_balance_amount: nil}

    test "return valid changeset with valid attributes" do
      changeset =
        BalanceTransaction.decrease_balance_changeset(%BalanceTransaction{}, @valid_attrs)

      balance_transaction = Changeset.apply_changes(changeset)

      assert changeset.valid?
      assert balance_transaction.type == :transfer
      assert balance_transaction.status == :pending
      assert balance_transaction.operation_code == "operation-12345"
      assert Decimal.eq?(balance_transaction.amount, -200)
      assert Decimal.eq?(balance_transaction.current_balance_amount, 2_000)
    end

    test "return invalid changeset with invalid attributes" do
      changeset =
        BalanceTransaction.decrease_balance_changeset(%BalanceTransaction{}, @invalid_attrs)

      refute changeset.valid?

      assert %{
               type: ["can't be blank"],
               operation_code: ["can't be blank"],
               current_balance_amount: ["can't be blank"]
             } == errors_on(changeset)
    end

    test "return invalid changeset if amount is greater than current balance amount" do
      changeset =
        BalanceTransaction.decrease_balance_changeset(%BalanceTransaction{}, %{
          @valid_attrs
          | current_balance_amount: 100
        })

      refute changeset.valid?

      assert %{amount: ["must be less than or equal to 100"]} == errors_on(changeset)
    end
  end

  describe "update_operations_from_transfer/2" do
    @valid_attrs %{
      amount: 1_000,
      type: :transfer,
      operation_code: "operation-12345",
      current_balance_amount: 2_000
    }

    test "return group of operations with valid changesets if attrs are valid" do
      destination_changeset =
        BalanceTransaction.increase_balance_changeset(%BalanceTransaction{}, @valid_attrs)

      destination = Changeset.apply_changes(destination_changeset)

      source_changeset =
        BalanceTransaction.increase_balance_changeset(%BalanceTransaction{}, @valid_attrs)

      source = Changeset.apply_changes(source_changeset)

      operations = BalanceTransaction.update_operations_from_transfer(source, destination)

      assert [
               source_balance_transaction: {:insert, source_balance_changeset, []},
               destination_balance_transaction: {:insert, destination_balance_changeset, []}
             ] = Ecto.Multi.to_list(operations)

      assert source_balance_changeset.valid?
      assert destination_balance_changeset.valid?
    end
  end

  describe "update_operation_from_withdrawal/2" do
    @valid_attrs %{
      amount: 1_000,
      type: :transfer,
      operation_code: "operation-12345",
      current_balance_amount: 2_000
    }

    test "return group of operations with valid changesets if attrs are valid" do
      changeset =
        BalanceTransaction.decrease_balance_changeset(%BalanceTransaction{}, @valid_attrs)

      transaction = Changeset.apply_changes(changeset)

      operations = BalanceTransaction.update_operation_from_withdrawal(transaction)

      assert [
               balance_transaction: {:insert, changeset, []}
             ] = Ecto.Multi.to_list(operations)

      assert changeset.valid?
    end
  end
end
