# Banking

The `banking` application is responsible for managing banking operations in
the project.

We currently have two of them: `Banking.Withdrawal` and `Banking.Transfer`.

All of them work the same way. Each one has an `embedded_schema` in
`lib/banking`, which is responsible for validating the data from
an external request.

After that, a new `Banking.BalanceTransaction` is created with all the information from the banking operation. This transaction is associated with a `Balance`, and scheduled to be processed in predefined intervals in the `Banking.TransactionQueue` module. When processed, the associated balance and the transaction will be updated on database.

**Important:** Two transactions will be created for each transfer. One for the
source balance and other for the destination balance. Both of them will have
the same `operation_code` attribute.

A transaction with the type `:withdrawal` will have a negative amount, and
its value will be subtracted from its associated balance amount.

An operation with the type `:transfer` will generate two transactions with the
same token - one with negative amount and another with a positive one. If one
update fails, the other will too.

It's dependent on the `data_vault` application from the umbrella.
"""

## Installation

If a given application under umbrella needs the Banking application, it should be added as an umbrella dependency into mix.exs of the given app.

```elixir
def deps do
  [
    {:banking, in_umbrella: true}
  ]
end
```
