# DataVault

The application `data_vault` contains the database repository to be added as a dependency in all the others applications under the umbrella.

It uses `Ecto.Repo` to do the database management and also contains all the migrations for the application.

## Installation

If a given application under umbrella needs the DataVault application, it should be added as an umbrella dependency into mix.exs of the given app.

```elixir
def deps do
  [
    {:data_vault, in_umbrella: true}
  ]
end
```

It also needs to be added as the `ecto_repo` of the given application in `config/config.exs`

```elixir
config :app, :ecto_repos, [DataVault.Repo]
```
