defmodule DataVault do
  @moduledoc """
  The module `DataVault` is the database repository to be added as a dependency
  in all the others applications under the umbrella.

  It uses Ecto.Repo to do the database management.

  It also contains the migrations for the application.
  """
end
