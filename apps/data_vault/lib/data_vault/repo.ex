defmodule DataVault.Repo do
  @moduledoc false

  use Ecto.Repo,
    otp_app: :data_vault,
    adapter: Ecto.Adapters.Postgres
end
