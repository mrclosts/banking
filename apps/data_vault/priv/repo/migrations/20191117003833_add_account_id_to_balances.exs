defmodule DataVault.Repo.Migrations.AddAccountIdToBalances do
  use Ecto.Migration

  def change do
    alter table("balances") do
      add(:account_id, references(:accounts, on_delete: :delete_all), null: false)
    end

    create unique_index("balances", [:account_id])
  end
end
