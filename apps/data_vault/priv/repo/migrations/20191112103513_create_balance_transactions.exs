defmodule DataVault.Repo.Migrations.CreateBalanceTransactions do
  use Ecto.Migration

  def change do
    BalanceTransactionTypeEnum.create_type
    BalanceTransactionStatusEnum.create_type

    create table(:balance_transactions) do
      add :amount, :decimal, precision: 10, scale: 2, null: false
      add :type, BalanceTransactionTypeEnum.type(), null: false
      add :status, BalanceTransactionStatusEnum.type(), null: false

      timestamps()
    end
  end
end
