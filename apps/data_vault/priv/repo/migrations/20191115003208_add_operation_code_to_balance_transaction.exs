defmodule DataVault.Repo.Migrations.AddOperationCodeToBalanceTransaction do
  use Ecto.Migration

  def change do
    alter table(:balance_transactions) do
      add :operation_code, :string, null: false
    end
  end
end
