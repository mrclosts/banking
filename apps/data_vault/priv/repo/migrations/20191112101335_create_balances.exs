defmodule DataVault.Repo.Migrations.CreateBalances do
  use Ecto.Migration

  def change do
    create table(:balances) do
      add :amount, :decimal, precision: 10, scale: 2, null: false, default: 0

      timestamps()
    end
  end
end
