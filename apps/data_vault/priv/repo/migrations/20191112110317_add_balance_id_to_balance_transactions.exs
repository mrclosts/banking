defmodule DataVault.Repo.Migrations.AddBalanceIdToBalanceTransactions do
  use Ecto.Migration

  def change do
    alter table(:balance_transactions) do
      add :balance_id, references(:balances, on_delete: :delete_all), null: false
    end
  end
end
