defmodule DataVault.Repo.Migrations.AddAccountIdToProfiles do
  use Ecto.Migration

  def change do
    alter table("profiles") do
      add :account_id, references(:accounts, on_delete: :delete_all), null: false
    end
  end
end
