# API

The `api` app under the umbrella project manages the web API of the application. It's a Phoenix project providing a JSON API.

It has access to all other applications of the umbrella, as it needs them to
build the responses for each requests.

## Accounts

Creating a new account in the application:

```bash
curl -XPOST -H "Content-type: application/json" -d '{
  "name": "John Baskerville",
  "password": "123456",
  "email": "john@mail.com"
}' 'localhost:4000/api/accounts/registrations'
```

```json
{
  "data":{
    "current_balance_amount":"R$ 1.000,00",
    "email":"john@mail.com",
    "token":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhY2NvdW50cyIsImV4cCI6MTU3NzU1NjQxNiwiaWF0IjoxNTc1MTM3MjE2LCJpc3MiOiJhY2NvdW50cyIsImp0aSI6ImE1NGI4ZjVmLTE4ODUtNGJiYy04ZGJkLTMwNmE5ZmFjNzZiNiIsIm5iZiI6MTU3NTEzNzIxNSwic3ViIjoiam9obkBtYWlsLmNvbSIsInR5cCI6ImFjY2VzcyJ9.lPdTV5yRjdOL70I2SnVUnrykopiViKN26srLozIZlMUmjtkHoZyxlDC12xYFLli8hO8B9VYXE5JJIWOt-3Uy2w"
  }
}
```

```json
{
  "errors":{
    "email":[
      "can't be blank"
    ],
    "name":[
      "can't be blank"
    ],
    "password":[
      "can't be blank"
    ]
  }
}
```

Authenticating an account into the application:

```bash
curl -XPOST -H "Content-type: application/json" -d '{"email": "john@mail.com", "password": "123456"}' 'localhost:4000/api/accounts/sessions'
```

```json
{
  "data":{
    "current_balance_amount":"R$ 1.000,00",
    "email":"john@mail.com",
    "token":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhY2NvdW50cyIsImV4cCI6MTU3NzU1NzMwOSwiaWF0IjoxNTc1MTM4MTA5LCJpc3MiOiJhY2NvdW50cyIsImp0aSI6IjE4MDhiNjI4LTBlNDUtNGYyNi1hMWZlLTEwYTk2NzZiZjVkNiIsIm5iZiI6MTU3NTEzODEwOCwic3ViIjoiam9obkBtYWlsLmNvbSIsInR5cCI6ImFjY2VzcyJ9._zgHaDXhNWDuK_8qWjFd3QCQB0HcK4lVUaljkRHkNUTBRiaeHFS5uVdJyPwZno4ZgWC005W-MAraOOiqf8UwEQ"
  }
}
```

```json
{
  "errors":{
    "detail":"Invalid email/password combination."
  }
}
```

## Banking

From here, all requests must have the `Authorization` header with the value `Bearer authentication_token`. This token will be returned when
signing in or registrating into the application.

Transfering money:

```bash
curl -XPOST -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhY2NvdW50cyIsImV4cCI6MTU3NzU1NzMwOSwiaWF0IjoxNTc1MTM4MTA5LCJpc3MiOiJhY2NvdW50cyIsImp0aSI6IjE4MDhiNjI4LTBlNDUtNGYyNi1hMWZlLTEwYTk2NzZiZjVkNiIsIm5iZiI6MTU3NTEzODEwOCwic3ViIjoiam9obkBtYWlsLmNvbSIsInR5cCI6ImFjY2VzcyJ9._zgHaDXhNWDuK_8qWjFd3QCQB0HcK4lVUaljkRHkNUTBRiaeHFS5uVdJyPwZno4ZgWC005W-MAraOOiqf8UwEQ' -H "Content-type: application/json" -d '{
  "destination": "charlie@mail.com",
  "amount": 200
}' 'localhost:4000/api/banking/operations/transfer'
```

```json
{
  "data":{
    "destination_balance_transaction":{
      "amount":"R$ 200,00",
      "operation_code":"e5621eae-1f27-4db9-84ce-2b87e1901f6d",
      "status":"pending",
      "type":"transfer"
    },
    "message":"Your operation was performed and the vinculated balances will be updated shortly.",
    "source_balance_transaction":{
      "amount":"-R$ 200,00",
      "operation_code":"e5621eae-1f27-4db9-84ce-2b87e1901f6d",
      "status":"pending",
      "type":"transfer"
    }
  }
}
```

```json
{
  "errors":{
    "detail":"You must sign in to perform this operation."
  }
}
```

Withdrawal money:

```bash
curl -XPOST -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhY2NvdW50cyIsImV4cCI6MTU3NzU1NzMwOSwiaWF0IjoxNTc1MTM4MTA5LCJpc3MiOiJhY2NvdW50cyIsImp0aSI6IjE4MDhiNjI4LTBlNDUtNGYyNi1hMWZlLTEwYTk2NzZiZjVkNiIsIm5iZiI6MTU3NTEzODEwOCwic3ViIjoiam9obkBtYWlsLmNvbSIsInR5cCI6ImFjY2VzcyJ9._zgHaDXhNWDuK_8qWjFd3QCQB0HcK4lVUaljkRHkNUTBRiaeHFS5uVdJyPwZno4ZgWC005W-MAraOOiqf8UwEQ' -H "Content-type: application/json" -d '{
>   "amount": 200
> }' 'localhost:4000/api/banking/operations/withdrawal'
```

```json
{
  "data":{
    "balance_transaction":{
      "amount":"-R$ 200,00",
      "operation_code":"c796bb5b-6502-4b73-8615-826763e18e4c",
      "status":"pending",
      "type":"withdrawal"
    },
    "message":"Your operation was performed and the vinculated balance will be updated shortly."
  }
}
```

```json
{
  "errors":{
    "detail":"You must sign in to perform this operation."
  }
}
```

Banking Transactions report:

```bash
curl -XGET -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhY2NvdW50cyIsImV4cCI6MTU3NzU1NzMwOSwiaWF0IjoxNTc1MTM4MTA5LCJpc3MiOiJhY2NvdW50cyIsImp0aSI6IjE4MDhiNjI4LTBlNDUtNGYyNi1hMWZlLTEwYTk2NzZiZjVkNiIsIm5iZiI6MTU3NTEzODEwOCwic3ViIjoiam9obkBtYWlsLmNvbSIsInR5cCI6ImFjY2VzcyJ9._zgHaDXhNWDuK_8qWjFd3QCQB0HcK4lVUaljkRHkNUTBRiaeHFS5uVdJyPwZno4ZgWC005W-MAraOOiqf8UwEQ' -H "Content-type: application/json" 'localhost:4000/api/banking/report'
```

```json
{
  "current_month":"R$ 400,00",
  "current_year":"R$ 400,00",
  "today":"R$ 400,00",
  "total":"R$ 400,00"
}
```
