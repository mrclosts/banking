defmodule ApiWeb.Accounts.RegistrationsControllerTest do
  use DataVault.RepoCase
  use ApiWeb.ConnCase

  describe "create/2" do
    @valid_attrs %{name: "John Baskerville", password: "123456", email: "john@mail.com"}
    @invalid_attrs %{name: nil, password: nil, email: nil}

    test "returns account information with valid attrs", %{conn: conn} do
      response =
        conn
        |> post(Routes.registrations_path(conn, :create, @valid_attrs))
        |> json_response(200)

      assert response["data"]
      assert response["data"]["email"] == "john@mail.com"
      assert response["data"]["current_balance_amount"] == "R$ 1.000,00"
      assert response["data"]["token"]
    end

    test "returns errors with invalid attrs", %{conn: conn} do
      response =
        conn
        |> post(Routes.registrations_path(conn, :create, @invalid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "email" => ["can't be blank"],
          "name" => ["can't be blank"],
          "password" => ["can't be blank"]
        }
      }

      assert response == expected
    end

    test "returns error when account with given email already exists", %{conn: conn} do
      Accounts.create_account(@valid_attrs)

      response =
        conn
        |> post(Routes.registrations_path(conn, :create, @valid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "email" => ["has already been taken"]
        }
      }

      assert response == expected
    end
  end
end
