defmodule ApiWeb.Accounts.SessionsControllerTest do
  use DataVault.RepoCase
  use ApiWeb.ConnCase

  import Accounts.Factory

  describe "create/2" do
    @valid_attrs %{email: "john@mail.com", password: "123456"}
    @invalid_attrs %{email: nil, password: nil}

    test "returns account and auth token if login is successful", %{conn: conn} do
      account = insert!(:valid_account, email: "john@mail.com")
      {:ok, _} = Banking.create_balance_for_account(account.id)

      response =
        conn
        |> post(Routes.sessions_path(conn, :create, @valid_attrs))
        |> json_response(200)

      assert response["data"]
      assert response["data"]["email"] == "john@mail.com"
      assert response["data"]["current_balance_amount"] == "R$ 1.000,00"
      assert response["data"]["token"]
    end

    test "returns error on login with invalid attrs", %{conn: conn} do
      response =
        conn
        |> post(Routes.sessions_path(conn, :create, @invalid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "email" => ["can't be blank"],
          "password" => ["can't be blank"]
        }
      }

      assert response == expected
    end

    test "returns error if there's no account for the given email/password combination", %{
      conn: conn
    } do
      response =
        conn
        |> post(Routes.sessions_path(conn, :create, @valid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "detail" => "Invalid email/password combination."
        }
      }

      assert response == expected
    end
  end
end
