defmodule ApiWeb.Banking.ReportsControllerTest do
  use DataVault.RepoCase
  use ApiWeb.ConnCase

  import Accounts.{Factory, Guardian}

  setup %{conn: conn} do
    account = insert!(:valid_account)
    {:ok, token, _} = encode_and_sign(account, %{}, token_type: :access)
    authorized_conn = put_req_header(conn, "authorization", "bearer: " <> token)
    %{authorized_conn: authorized_conn, unauthorized_conn: conn, account: account}
  end

  describe "report/2" do
    test "return report for `today`, `current_month`, `current_year` and `total`", %{
      authorized_conn: conn,
      account: account
    } do
      today = truncate_naive_date_time(NaiveDateTime.utc_now())
      {:ok, same_month} = NaiveDateTime.new(today.year, today.month, 1, 0, 0, 0)
      same_month = truncate_naive_date_time(same_month)
      {:ok, same_year} = NaiveDateTime.new(today.year, 1, 1, 0, 0, 0)
      same_year = truncate_naive_date_time(same_year)
      {:ok, a_year_ago} = NaiveDateTime.new(today.year - 1, today.month, 1, 0, 0, 0)
      a_year_ago = truncate_naive_date_time(a_year_ago)

      {:ok, balance} = Banking.create_balance_for_account(account.id)

      DataVault.Repo.insert_all(
        Banking.BalanceTransaction,
        [
          %{
            amount: -100,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: today,
            updated_at: today
          },
          %{
            amount: 300,
            type: :transfer,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: same_month,
            updated_at: today
          },
          %{
            amount: -100,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: same_month,
            updated_at: today
          },
          %{
            amount: -100,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: same_year,
            updated_at: today
          },
          %{
            amount: -200,
            type: :withdrawal,
            status: :pending,
            operation_code: Ecto.UUID.generate(),
            balance_id: balance.id,
            inserted_at: a_year_ago,
            updated_at: today
          }
        ]
      )

      response =
        conn
        |> get(Routes.reports_path(conn, :report))
        |> json_response(200)

      expected = %{
        "today" => "R$ 100,00",
        "current_month" => "R$ 500,00",
        "current_year" => "R$ 600,00",
        "total" => "R$ 800,00"
      }

      assert response == expected
    end

    test "return empty report", %{authorized_conn: conn} do
      response =
        conn
        |> get(Routes.reports_path(conn, :report))
        |> json_response(200)

      expected = %{
        "today" => "R$ 0,00",
        "current_month" => "R$ 0,00",
        "current_year" => "R$ 0,00",
        "total" => "R$ 0,00"
      }

      assert response == expected
    end

    test "return unauthenticated error when attempting to fetch report without being authenticated",
         %{unauthorized_conn: conn} do
      response =
        conn
        |> get(Routes.reports_path(conn, :report))
        |> json_response(200)

      assert response
    end
  end
end
