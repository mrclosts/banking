defmodule ApiWeb.Banking.OperationsControllerTest do
  use DataVault.RepoCase
  use ApiWeb.ConnCase

  import Accounts.{Factory, Guardian}

  setup %{conn: conn} do
    account = insert!(:valid_account)
    {:ok, token, _} = encode_and_sign(account, %{}, token_type: :access)
    authorized_conn = put_req_header(conn, "authorization", "bearer: " <> token)
    %{authorized_conn: authorized_conn, unauthorized_conn: conn, account: account}
  end

  describe "transfer/3" do
    @valid_attrs %{amount: 200, destination: "charlie@mail.com"}
    @invalid_attrs %{amount: nil, destination: nil}

    test "returns unauthenticated error when attempting to perform a transfer without being authenticated",
         %{unauthorized_conn: conn} do
      response =
        conn
        |> post(Routes.operations_path(conn, :transfer, @valid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "detail" => "You must sign in to perform this operation."
        }
      }

      assert expected == response
    end

    test "returns source and destination transaction informations if operation is successful", %{
      authorized_conn: conn,
      account: source_account
    } do
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      Banking.create_balance_for_account(source_account.id)
      Banking.create_balance_for_account(destination_account.id)

      response =
        conn
        |> post(Routes.operations_path(conn, :transfer, @valid_attrs))
        |> json_response(200)

      assert is_map(response)

      operation_code = response["data"]["source_balance_transaction"]["operation_code"]

      expected = %{
        "data" => %{
          "source_balance_transaction" => %{
            "amount" => "-R$ 200,00",
            "type" => "transfer",
            "status" => "pending",
            "operation_code" => operation_code
          },
          "destination_balance_transaction" => %{
            "amount" => "R$ 200,00",
            "type" => "transfer",
            "status" => "pending",
            "operation_code" => operation_code
          },
          "message" =>
            "Your operation was performed and the vinculated balances will be updated shortly."
        }
      }

      assert response == expected
    end

    test "returns errors when receiving invalid attrs", %{authorized_conn: conn} do
      response =
        conn
        |> post(Routes.operations_path(conn, :transfer, @invalid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "amount" => ["can't be blank"],
          "destination" => ["can't be blank"]
        }
      }

      assert expected == response
    end

    test "returns errors when receiving negative amount", %{
      authorized_conn: conn,
      account: source_account
    } do
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      Banking.create_balance_for_account(source_account.id)
      Banking.create_balance_for_account(destination_account.id)

      response =
        conn
        |> post(Routes.operations_path(conn, :transfer, %{@valid_attrs | amount: -50}))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "amount" => ["must be greater than 0"]
        }
      }

      assert expected == response
    end

    test "returns error when destination account doesn't exist", %{
      authorized_conn: conn,
      account: source_account
    } do
      Banking.create_balance_for_account(source_account.id)

      response =
        conn
        |> post(Routes.operations_path(conn, :transfer, @valid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "detail" => "There's no balance for the account."
        }
      }

      assert expected == response
    end

    test "returns error when receiving amount greater than the current balance amount", %{
      authorized_conn: conn,
      account: source_account
    } do
      destination_account = insert!(:valid_account, email: "charlie@mail.com")

      Banking.create_balance_for_account(source_account.id)
      Banking.create_balance_for_account(destination_account.id)

      response =
        conn
        |> post(Routes.operations_path(conn, :transfer, %{@valid_attrs | amount: 5_000}))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "amount" => ["must be less than or equal to 1000.00"]
        }
      }

      assert expected == response
    end
  end

  describe "withdrawal/3" do
    @valid_attrs %{amount: 200}
    @invalid_attrs %{amount: nil}

    test "returns unauthenticated error when attempting to perform a withdrawal without being authenticated",
         %{unauthorized_conn: conn} do
      response =
        conn
        |> post(Routes.operations_path(conn, :withdrawal, @valid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "detail" => "You must sign in to perform this operation."
        }
      }

      assert expected == response
    end

    test "returns withdrawal transaction information if operation is successful", %{
      authorized_conn: conn,
      account: account
    } do
      Banking.create_balance_for_account(account.id)

      response =
        conn
        |> post(Routes.operations_path(conn, :withdrawal, @valid_attrs))
        |> json_response(200)

      assert is_map(response)

      operation_code = response["data"]["balance_transaction"]["operation_code"]

      expected = %{
        "data" => %{
          "balance_transaction" => %{
            "amount" => "-R$ 200,00",
            "type" => "withdrawal",
            "status" => "pending",
            "operation_code" => operation_code
          },
          "message" =>
            "Your operation was performed and the vinculated balance will be updated shortly."
        }
      }

      assert response == expected
    end

    test "returns errors when receiving invalid attrs", %{authorized_conn: conn} do
      response =
        conn
        |> post(Routes.operations_path(conn, :withdrawal, @invalid_attrs))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "amount" => ["can't be blank"]
        }
      }

      assert expected == response
    end

    test "returns errors when receiving negative amount", %{
      authorized_conn: conn,
      account: account
    } do
      Banking.create_balance_for_account(account.id)

      response =
        conn
        |> post(Routes.operations_path(conn, :withdrawal, %{@valid_attrs | amount: -50}))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "amount" => ["must be greater than 0"]
        }
      }

      assert expected == response
    end

    test "returns error when receiving amount greater than the current balance amount", %{
      authorized_conn: conn,
      account: account
    } do
      Banking.create_balance_for_account(account.id)

      response =
        conn
        |> post(Routes.operations_path(conn, :withdrawal, %{@valid_attrs | amount: 5_000}))
        |> json_response(200)

      expected = %{
        "errors" => %{
          "amount" => ["must be less than or equal to 1000.00"]
        }
      }

      assert expected == response
    end
  end
end
