defmodule ApiWeb.Banking.ReportsController do
  @moduledoc """
  Controller for banking reports.

  It responds to GET requests to `/banking/report` and returns the total amount of
  a series of transactions from the current_day, current_month, current_year and
  the total amount already transacted.
  """

  use ApiWeb, :controller

  action_fallback ApiWeb.FallbackController

  @doc false
  def report(conn, _params) do
    render(conn, "report.json", report: Banking.generate_report())
  end
end
