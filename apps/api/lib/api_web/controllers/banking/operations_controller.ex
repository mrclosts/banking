defmodule ApiWeb.Banking.OperationsController do
  @moduledoc """
  Controller for banking operations.

  It responds to POST requests to `/banking/transfer`, for transfer operations and
  POST requests to `/banking/withdrawal` for withdrawal operations.
  """

  use ApiWeb, :controller

  action_fallback ApiWeb.FallbackController

  @doc false
  def transfer(conn, params, account) do
    with {:ok, {source_transaction, destination_transaction} = transactions} <-
           Banking.transfer_money(account, params),
         {:ok, :balance_update_scheduled} <- Banking.schedule_balance_update(transactions),
         {:ok, _} <-
           Mailer.notify_banking_operation_to(%{
             email: account.email,
             resource: source_transaction
           }),
         {:ok, _} <-
           Mailer.notify_banking_operation_to(%{
             email: params["destination"],
             resource: destination_transaction
           }) do
      render(conn, "transfer.json", transactions: transactions)
    end
  end

  @doc false
  def withdrawal(conn, params, account) do
    with {:ok, withdrawal_transaction} <- Banking.withdrawal_money(account, params),
         {:ok, :balance_update_scheduled} <-
           Banking.schedule_balance_update(withdrawal_transaction),
         {:ok, _} <-
           Mailer.notify_banking_operation_to(%{
             email: account.email,
             resource: withdrawal_transaction
           }) do
      render(conn, "withdrawal.json", balance_transaction: withdrawal_transaction)
    end
  end

  defp action(conn, _) do
    account = Guardian.Plug.current_resource(conn)
    args = [conn, conn.params, account]
    apply(__MODULE__, action_name(conn), args)
  end
end
