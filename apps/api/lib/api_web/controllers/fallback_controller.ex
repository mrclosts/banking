defmodule ApiWeb.FallbackController do
  @moduledoc """
  Module for catching common errors of the controllers from the application.
  """

  use ApiWeb, :controller
  alias ApiWeb.ErrorView

  @doc false
  def call(conn, {:error, %Ecto.Changeset{valid?: false} = changeset}) do
    conn
    |> put_view(ErrorView)
    |> render("error.json", changeset: changeset)
  end

  @doc false
  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_view(ErrorView)
    |> render("error.json", error: "Invalid email/password combination.")
  end

  @doc false
  def call(conn, {:error, :balance_not_found}) do
    conn
    |> put_view(ErrorView)
    |> render("error.json", error: "There's no balance for the account.")
  end

  @doc false
  def call(conn, _error) do
    conn
    |> put_view(ErrorView)
    |> render("500.json")
  end

  @doc false
  @behaviour Guardian.Plug.ErrorHandler
  def auth_error(conn, {type, _reason}, _opts) do
    conn
    |> put_view(ErrorView)
    |> render("error.json", error: type)
  end
end
