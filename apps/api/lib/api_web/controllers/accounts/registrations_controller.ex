defmodule ApiWeb.Accounts.RegistrationsController do
  @moduledoc """
  Controller for registrating a new account via external request.

  It responds to POST requests to `/accounts/registrations`.
  """

  use ApiWeb, :controller

  action_fallback ApiWeb.FallbackController

  @doc false
  def create(conn, account_params) do
    with {:ok, account, profile} <- Accounts.create_account(account_params),
         {:ok, balance} <- Banking.create_balance_for_account(account.id),
         {:ok, token, _} <- Accounts.authenticate(account_params),
         {:ok, _} <-
           Mailer.send_welcome_mail_to(%{
             email: account.email,
             name: profile.name,
             current_balance_amount: balance.amount
           }) do
      conn
      |> put_view(ApiWeb.AccountsView)
      |> render("account.json",
        account: account,
        token: token,
        current_balance_amount: balance.amount
      )
    end
  end
end
