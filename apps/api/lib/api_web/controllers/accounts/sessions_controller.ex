defmodule ApiWeb.Accounts.SessionsController do
  @moduledoc """
  Controller for authenticating a new account via external request.

  It responds to POST requests to `/accounts/sessions`.
  """

  use ApiWeb, :controller

  action_fallback ApiWeb.FallbackController

  def create(conn, params) do
    with {:ok, token, account} <- Accounts.authenticate(params),
         %Banking.Balance{} = balance <-
           Banking.get_balance_from_account_by(%{account_id: account.id}) do
      conn
      |> put_view(ApiWeb.AccountsView)
      |> render("account.json",
        account: account,
        token: token,
        current_balance_amount: balance.amount
      )
    end
  end
end
