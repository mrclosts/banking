defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authentication do
    plug ApiWeb.Pipeline
  end

  pipeline :ensure_authentication do
    plug Guardian.Plug.EnsureAuthenticated
  end

  scope "/api", ApiWeb do
    pipe_through [:api, :authentication]

    scope "/accounts", Accounts do
      resources "/registrations", RegistrationsController, only: [:create]
      resources "/sessions", SessionsController, only: [:create]
    end

    scope "/banking", Banking do
      pipe_through :ensure_authentication

      scope "/operations" do
        post "/transfer", OperationsController, :transfer
        post "/withdrawal", OperationsController, :withdrawal
      end

      get "/report", ReportsController, :report
    end
  end
end
