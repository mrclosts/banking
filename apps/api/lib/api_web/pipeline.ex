defmodule ApiWeb.Pipeline do
  @moduledoc false
  use Guardian.Plug.Pipeline,
    otp_app: :accounts,
    error_handler: ApiWeb.FallbackController,
    module: Accounts.Guardian

  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
  plug Guardian.Plug.LoadResource, allow_blank: true
end
