defmodule ApiWeb.ErrorView do
  @moduledoc false
  use ApiWeb, :view

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.json" becomes
  # "Not Found".
  def template_not_found(template, _assigns) do
    %{errors: %{detail: Phoenix.Controller.status_message_from_template(template)}}
  end

  # If you want to customize a particular status code
  # for a certain format, you may uncomment below.
  def render("500.json", _assigns) do
    %{errors: %{detail: "Internal Server Error"}}
  end

  @doc """
  Deals with errors on changeset.
  """
  def render("error.json", %{changeset: changeset}) do
    %{errors: errors_on(changeset)}
  end

  @doc """
  Deals with :unauthenticated error on Guardian.
  """
  def render("error.json", %{error: :unauthenticated}) do
    %{errors: %{detail: "You must sign in to perform this operation."}}
  end

  @doc """
  Deals with :unauthorized error on Guardian.
  """
  def render("error.json", %{error: :unauthorized}) do
    %{errors: %{detail: "You're not authorized to do this operation."}}
  end

  @doc """
  Deals with :invalid_token error on Guardian.
  """
  def render("error.json", %{error: :invalid_token}) do
    %{errors: %{detail: "Your authentication token is invalid."}}
  end

  @doc """
  Deals with :already_authenticated error on Guardian.
  """
  def render("error.json", %{error: :already_authenticated}) do
    %{errors: %{detail: "You're already authenticated."}}
  end

  @doc """
  Deals with :no_resource_found error on Guardian.
  """
  def render("error.json", %{error: :no_resource_found}) do
    %{errors: %{detail: "There's no account with the token provided."}}
  end

  @doc """
  Deals with message errors.
  """
  def render("error.json", %{error: error_message}) do
    %{errors: %{detail: error_message}}
  end

  defp errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Enum.reduce(opts, message, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end
end
