defmodule ApiWeb.AccountsView do
  @moduledoc false
  use ApiWeb, :view

  import Number.Currency

  def render("account.json", %{
        account: account,
        token: token,
        current_balance_amount: current_balance_amount
      }) do
    %{
      data: %{
        email: account.email,
        token: token,
        current_balance_amount: number_to_currency(current_balance_amount)
      }
    }
  end
end
