defmodule ApiWeb.Banking.OperationsView do
  @moduledoc false
  use ApiWeb, :view

  import Number.Currency

  def render("transfer.json", %{transactions: {source_transaction, destination_transaction}}) do
    %{
      data: %{
        source_balance_transaction:
          render_one(
            source_transaction,
            __MODULE__,
            "balance_transaction.json",
            balance_transaction: source_transaction
          ),
        destination_balance_transaction:
          render_one(
            destination_transaction,
            __MODULE__,
            "balance_transaction.json",
            balance_transaction: destination_transaction
          ),
        message:
          "Your operation was performed and the vinculated balances will be updated shortly."
      }
    }
  end

  def render("withdrawal.json", %{balance_transaction: transaction}) do
    %{
      data: %{
        balance_transaction:
          render_one(
            transaction,
            __MODULE__,
            "balance_transaction.json",
            balance_transaction: transaction
          ),
        message:
          "Your operation was performed and the vinculated balance will be updated shortly."
      }
    }
  end

  def render("balance_transaction.json", %{balance_transaction: balance_transaction}) do
    %{
      amount: number_to_currency(balance_transaction.amount),
      type: balance_transaction.type,
      status: balance_transaction.status,
      operation_code: balance_transaction.operation_code
    }
  end
end
