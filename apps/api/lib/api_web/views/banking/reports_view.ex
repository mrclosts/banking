defmodule ApiWeb.Banking.ReportsView do
  @moduledoc false
  use ApiWeb, :view

  import Number.Currency

  def render("report.json", %{report: report}) do
    %{
      today: number_to_currency(report.today || 0),
      current_month: number_to_currency(report.current_month || 0),
      current_year: number_to_currency(report.current_year || 0),
      total: number_to_currency(report.total || 0)
    }
  end
end
