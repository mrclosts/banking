defmodule Mailer.QueueTest do
  use ExUnit.Case

  test "receives :mail_delivery message for delivering the emails from queue" do
    assert :ok = Mailer.Queue.add("Mail.")

    children = Supervisor.which_children(Mailer.Supervisor)

    assert {Mailer.Queue, pid, _, _} = List.keyfind(children, Mailer.Queue, 0)

    :erlang.trace(pid, true, [:receive])
    assert_receive {:trace, ^pid, :receive, :mail_delivery}, 500
  end
end
