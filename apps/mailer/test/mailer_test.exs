defmodule MailerTest do
  use ExUnit.Case

  describe "send_welcome_mail_to/1" do
    @valid_options %{
      email: "john@mail.com",
      name: "John Baskerville",
      current_balance_amount: "R$ 1.000,00"
    }
    @invalid_options %{email: nil, name: nil, current_balance_amount: nil}

    test "adds welcome email to mailer queue" do
      assert {:ok, :email_enqueued} = Mailer.send_welcome_mail_to(@valid_options)

      assert Mailer.Queue.scheduled?(%{
               to: "john@mail.com",
               message: "Welcome, John Baskerville!",
               current_balance_amount: "R$ 1.000,00"
             })
    end

    test "returns error with invalid email attributes" do
      assert {:error, :invalid_email_options} = Mailer.send_welcome_mail_to(@invalid_options)
    end
  end

  describe "notify_banking_operation_to/1" do
    @valid_options %{email: "john@mail.com", resource: %{operation: "withdrawal"}}
    @invalid_options %{email: nil, resource: nil}

    test "adds banking operation email to mailer queue" do
      assert {:ok, :email_enqueued} = Mailer.notify_banking_operation_to(@valid_options)

      assert Mailer.Queue.scheduled?(%{
               to: "john@mail.com",
               resource: %{operation: "withdrawal"},
               message: "Operation performed. Your balance will be updated shortly."
             })
    end

    test "returns error with invalid email attributes" do
      assert {:error, :invalid_email_options} =
               Mailer.notify_banking_operation_to(@invalid_options)
    end
  end
end
