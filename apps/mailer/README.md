# Mailer

This application is responsible for managing the mail deliveries for the
umbrella app.

## Installation

If a given application under umbrella needs the Mailer application, it should be
added as an umbrella dependency into mix.exs of the given app.

```elixir
def deps do
  [
    {:mailer, in_umbrella: true}
  ]
end
```
