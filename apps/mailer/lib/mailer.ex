defmodule Mailer do
  @moduledoc """
  The module `Mailer` manages the email queue and deliveries for the application.

  It acts as the Public API for the `mailer` application and provides two functions:
  `send_welcome_mail_to/1` and `notify_banking_operation_to/1`.

  It's composed by the following modules: `Mailer.Application` and `Mailer.Queue`.

  The `Mailer.Queue` module enqueues all types of emails for scheduling their
  deliveries. It implements the GenServer behaviour for doing that.

  The `Mailer.Application` starts a Supervisor to supervise the queue.
  """

  @doc """
  Enqueue welcome mail to new account for delivery.
  """
  def send_welcome_mail_to(%{
        email: email,
        name: name,
        current_balance_amount: current_balance_amount
      })
      when not is_nil(email) and not is_nil(name) and not is_nil(current_balance_amount) do
    Mailer.Queue.add(%{
      to: email,
      message: "Welcome, #{name}!",
      current_balance_amount: current_balance_amount
    })

    {:ok, :email_enqueued}
  end

  def send_welcome_mail_to(_), do: {:error, :invalid_email_options}

  @doc """
  Enqueue mail for notifying a new banking operation.
  """
  def notify_banking_operation_to(%{email: email, resource: resource})
      when not is_nil(email) and not is_nil(resource) do
    Mailer.Queue.add(%{
      to: email,
      resource: resource,
      message: "Operation performed. Your balance will be updated shortly."
    })

    {:ok, :email_enqueued}
  end

  def notify_banking_operation_to(_), do: {:error, :invalid_email_options}
end
