defmodule Mailer.Queue do
  @moduledoc """
  Manages email deliveries for the application.
  It uses the GenServer behaviour for executing on defined schedules.
  """

  use GenServer
  require Logger

  @mailer_delivery_interval Application.get_env(:mailer, :mail_delivery_interval)

  @doc """
  Start the Mailer Queue with an empty queue.
  """
  def start_link(_defaults) do
    GenServer.start_link(__MODULE__, :queue.new(), name: __MODULE__)
  end

  def scheduled?(mail), do: GenServer.call(__MODULE__, {:scheduled?, mail})

  @doc """
  Add new email to the queue.
  """
  def add(email), do: GenServer.cast(__MODULE__, {:add, email})

  @doc """
  GenServer init/1 callback
  """
  @impl true
  def init(state) do
    schedule_mail_delivery()
    {:ok, state}
  end

  @impl true
  def handle_cast({:add, mail}, state) do
    {:noreply, :queue.in(mail, state)}
  end

  @impl true
  def handle_call({:scheduled?, mail}, _from, state) do
    {:reply, :queue.member(mail, state), state}
  end

  @impl true
  def handle_info(:mail_delivery, state) do
    log_mail_delivery("Start :mail_delivery - #{NaiveDateTime.utc_now()}")
    send_mail(:queue.out(state))
  end

  @doc """
  GenServer handle_info/2 catch-all callback.
  """
  @impl true
  def handle_info(_msg, state) do
    {:noreply, state}
  end

  defp schedule_mail_delivery do
    Process.send_after(self(), :mail_delivery, @mailer_delivery_interval)
  end

  defp send_mail({{:value, mail}, queue}) do
    if Mix.env() == :prod do
      IO.puts("Sending email...")
      IO.inspect(mail)
    end

    send_mail(:queue.out(queue))
  end

  defp send_mail({:empty, queue}) do
    log_mail_delivery("End :mail_delivery - #{NaiveDateTime.utc_now()}")
    schedule_mail_delivery()
    {:noreply, queue}
  end

  defp log_mail_delivery(message) do
    if Mix.env() == :prod do
      Logger.info(message)
    end
  end
end
