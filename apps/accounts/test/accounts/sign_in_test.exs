defmodule Accounts.SignInTest do
  use ExUnit.Case, async: true

  import DataVault.RepoCase, only: [errors_on: 1]

  alias Accounts.SignIn

  describe "changeset/1" do
    @valid_attrs %{email: "john@mail.com", password: "123456"}
    @invalid_attrs %{email: nil, password: nil}

    test "with valid data returns valid changeset" do
      changeset = SignIn.changeset(%SignIn{}, @valid_attrs)
      sign_in = Ecto.Changeset.apply_changes(changeset)

      assert changeset.valid?
      assert sign_in.email == "john@mail.com"
      assert sign_in.password == "123456"
    end

    test "with invalid data returns invalid changeset" do
      changeset = SignIn.changeset(%SignIn{}, @invalid_attrs)

      refute changeset.valid?
    end

    test "with invalid email returns invalid changeset" do
      changeset = SignIn.changeset(%SignIn{}, %{@valid_attrs | email: "john"})

      refute changeset.valid?
      assert %{email: ["has invalid format"]} = errors_on(changeset)
    end
  end
end
