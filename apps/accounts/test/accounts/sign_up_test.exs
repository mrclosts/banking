defmodule Accounts.SignUpTest do
  use ExUnit.Case, async: true

  import DataVault.RepoCase, only: [errors_on: 1]

  alias Accounts.SignUp

  describe "changeset/1" do
    @valid_attrs %{
      name: "John Baskerville",
      email: "john@mail.com",
      password: "123456",
      password_confirmation: "123456"
    }
    @invalid_attrs %{name: nil, email: nil, password: nil, password_confirmation: nil}

    test "with valid data returns valid changeset" do
      changeset = SignUp.changeset(%SignUp{}, @valid_attrs)
      sign_up = Ecto.Changeset.apply_changes(changeset)

      assert changeset.valid?
      assert sign_up.name == "John Baskerville"
      assert sign_up.email == "john@mail.com"
      assert sign_up.password == "123456"
    end

    test "with invalid data returns invalid changeset" do
      changeset = SignUp.changeset(%SignUp{}, @invalid_attrs)

      refute changeset.valid?
    end

    test "with invalid email returns invalid changeset" do
      changeset = SignUp.changeset(%SignUp{}, %{@valid_attrs | email: "john"})

      refute changeset.valid?
      assert %{email: ["has invalid format"]} = errors_on(changeset)
    end

    test "with small password length returns invalid changeset" do
      changeset = SignUp.changeset(%SignUp{}, %{@valid_attrs | password: "123"})

      refute changeset.valid?
      assert %{password: ["should be at least 6 character(s)"]} = errors_on(changeset)
    end

    test "with unmatching passwords returns invalid changeset" do
      changeset =
        SignUp.changeset(%SignUp{}, %{
          @valid_attrs
          | password: "123456",
            password_confirmation: "654321"
        })

      refute changeset.valid?
      assert %{password_confirmation: ["does not match password"]} = errors_on(changeset)
    end
  end
end
