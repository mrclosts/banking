defmodule Accounts.AccountTest do
  use DataVault.RepoCase, async: true

  import Accounts.Factory

  alias Accounts.Account

  describe "create_operations/1" do
    test "return operations group for creating profile account" do
      create_account_operations = Account.create_operations()

      assert [
               sign_up: {:run, _sign_up_changest_fun},
               account: {:run, _account_creation_fun},
               profile: {:run, _profile_creation_fun}
             ] = Ecto.Multi.to_list(create_account_operations)
    end
  end

  describe "check_password/2" do
    test "return success with valid password" do
      account = insert!(:valid_account)

      assert {:ok, _} = Account.check_password(account, "123456")
    end

    test "return error with invalid password" do
      account = insert!(:valid_account)

      assert {:error, _} = Account.check_password(account, "654321")
    end
  end
end
