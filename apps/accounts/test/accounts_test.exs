defmodule AccountsTest do
  use DataVault.RepoCase, async: true
  import Accounts.Factory

  describe "create_account/1" do
    @valid_attrs %{name: "John Baskerville", email: "john@mail.com", password: "123456"}
    @invalid_attrs %{name: nil, email: nil, password: nil}

    test "return new profile account if the attrs are valid" do
      assert {:ok, account, profile} = Accounts.create_account(@valid_attrs)

      assert account.email == "john@mail.com"
      assert profile.name == "John Baskerville"
      assert Pbkdf2.check_pass(account, "123456")
    end

    test "return error changeset if the attrs are invalid" do
      assert {:error, %Ecto.Changeset{valid?: false}} = Accounts.create_account(@invalid_attrs)
    end

    test "return error changeset if the email has already been taken" do
      insert!(:valid_account, email: @valid_attrs.email)

      assert {:error, changeset} = Accounts.create_account(@valid_attrs)
      assert %{email: ["has already been taken"]} = errors_on(changeset)
    end
  end

  describe "authenticate/1" do
    @valid_attrs %{email: "john@mail.com", password: "123456"}
    @invalid_attrs %{email: nil, password: nil}

    test "return account and token if the attrs are valid" do
      insert!(:valid_account, email: "john@mail.com")

      assert {:ok, token, _} = Accounts.authenticate(@valid_attrs)
      assert token
    end

    test "return error changeset if the required attrs are invalid" do
      assert {:error, %Ecto.Changeset{valid?: false}} = Accounts.authenticate(@invalid_attrs)
    end

    test "return error :unauthorized if the email/password combination is invalid" do
      insert!(:valid_account, email: "john@mail.com")

      assert {:error, :unauthorized} = Accounts.authenticate(%{@valid_attrs | password: "654321"})

      assert {:error, :unauthorized} =
               Accounts.authenticate(%{@valid_attrs | email: "charlie@mail.com"})
    end
  end

  describe "get_account_by_email/1" do
    test "return account if email is not nil" do
      account = insert!(:valid_account, email: "john@mail.com")
      result = Accounts.get_account_by_email("john@mail.com")

      assert account.id == result.id
      assert account.email == result.email
    end

    test "return nil if account with given email doesn't exist" do
      refute Accounts.get_account_by_email("john@mail.com")
    end

    test "return error if email is nil" do
      assert {:error, :invalid_email} = Accounts.get_account_by_email(nil)
    end
  end
end
