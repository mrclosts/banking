defmodule Accounts.Factory do
  @moduledoc false
  def build(:valid_account) do
    %Accounts.Account{
      profile: build(:profile),
      email: "mail#{System.unique_integer()}@mail.com",
      password_hash: Pbkdf2.hash_pwd_salt("123456")
    }
  end

  def build(:profile) do
    %Accounts.Profile{
      name: "John Baskerville"
    }
  end

  def build(factory_name, attributes) do
    factory_name
    |> build()
    |> struct(attributes)
  end

  def insert!(factory_name, attributes \\ []) do
    DataVault.Repo.insert!(build(factory_name, attributes))
  end
end
