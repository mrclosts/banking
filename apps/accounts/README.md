# Accounts

The `accounts` app under the umbrella project manages all the accounts.

It's responsible for dealing with account creation, authentication and
fetching and provides the Public API to the other applications of the umbrella
app. For those goals, its main module (`lib/accounts.ex`) provides three functions: `create_account/1`, `authenticate/1` and `get_account_by_email/1`.

It's composed by the following modules: `Accounts`, `Accounts.Account`, `Accounts.Guardian`, `Accounts.Profile`, `Accounts.SignIn`, and `Accounts.SignUp`.

The account's creation flow will first pass through the `Accounts.SignUp` module, which will validate the external attributes sent by the `ApiWeb.Accounts.RegistrationsController` module.
After that, the `Accounts.SignUp` fields will be used to persist the tables
from both `Accounts.Account` and `Accounts.Profile` schemas. It uses `Ecto.Multi` for this process to save all data in one database transaction. This is the ONLY way to create a new account into the app.

The account's authentication flow will first pass through the `Accounts.SignIn` module, which will validate the external attributes sent by the `ApiWeb.Accounts.SessionsController` module. After that, the `Accounts.SignIn` fields will be sent to `Accounts.Guardian` for generating a new authentication token to the client.

It's dependent on the `data_vault` application from the umbrella.

## Installation

If a given application under umbrella needs the Accounts application, it should be added as an umbrella dependency into mix.exs of the given app.

```elixir
def deps do
  [
    {:accounts, in_umbrella: true}
  ]
end
```
