defmodule Accounts.Profile do
  @moduledoc """
  The `Accounts.Profile` module contains the schema for the "profiles" table.

  This schema is associated with an Account and contains the fields related to
  the profile data of an account, such as its name.
  """

  use Ecto.Schema

  schema "profiles" do
    field(:name, :string, null: false)
    belongs_to(:account, Accounts.Account)

    timestamps()
  end
end
