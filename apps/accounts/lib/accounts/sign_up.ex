defmodule Accounts.SignUp do
  @moduledoc """
  The `Accounts.SignUp` provides an embedded_schema  for dealing with the
  validation of the fields required for creating a new account into the app.

  It's an embedded_schema because its only job is to validate external attributes.
  After the validation, its fields will be passed to `Accounts.Account` and
  `Accounts.Profile` to be persisted.

  As this module will be the only one to validate the sign up attributes,
  all attempts of creating a new account MUST pass through here first.
  """

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field(:name, :string)
    field(:email, :string)
    field(:password, :string)
  end

  @required_fields ~w(name email password)a

  def changeset(sign_up, attrs) do
    sign_up
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> validate_confirmation(:password, message: "does not match password")
    |> validate_length(:password, min: 6, max: 20)
    |> validate_format(:email, ~r/@/)
    |> update_change(:email, &String.downcase/1)
  end
end
