defmodule Accounts.Account do
  @moduledoc """
  The `Accounts.Account` module contains the schema for the "accounts" table, the
  changesets for creating a new account and the group of database transactions.
  """

  use Ecto.Schema

  import Ecto.Changeset

  alias Accounts.{Account, Profile, SignUp}
  alias Ecto.Multi

  schema "accounts" do
    field(:email, :string, null: false)
    field(:password_hash, :string, null: false)
    field(:password, :string, virtual: true)
    has_one(:profile, Profile)

    timestamps()
  end

  @doc """
  Group of operations for validating account creation using `Accounts.SignUp`,
  and then creating the account and the associated profile.
  """
  def create_operations(attrs \\ %{}) do
    Multi.new()
    |> Multi.run(:sign_up, fn _repo, _nothing ->
      changeset = SignUp.changeset(%SignUp{}, attrs)

      case changeset do
        %Ecto.Changeset{valid?: false} ->
          {:error, changeset}

        _ ->
          {:ok, changeset}
      end
    end)
    |> Multi.insert(:account, fn %{sign_up: sign_up} ->
      changeset(%Account{}, sign_up.changes)
    end)
    |> Multi.insert(:profile, fn %{account: account, sign_up: sign_up} ->
      Ecto.build_assoc(account, :profile, sign_up.changes)
    end)
  end

  @doc """
  Validates if a given password is the same of an `%Accounts.Account{}` struct.
  """
  def check_password(account, given_password) do
    Pbkdf2.check_pass(account, given_password)
  end

  defp changeset(account, attrs) do
    account
    |> cast(attrs, [:email, :password])
    |> put_password_hash()
    |> unique_constraint(:email)
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Pbkdf2.hash_pwd_salt(password))

      _ ->
        changeset
    end
  end
end
