defmodule Accounts.SignIn do
  @moduledoc """
  The `Accounts.SignIn` provides an embedded_schema for dealing with the
  validation of the fields required for authenticating into the app.

  It's an embedded_schema because its only job is to validate external attributes.
  After the validation, its fields will be passed to `Accounts.Guardian` to complete
  the authentication.

  As this module will be the only one to validate the authentication attributes,
  all attempts of authentication MUST pass through here first.
  """

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field(:email, :string)
    field(:password, :string)
  end

  @required_fields ~w(email password)a

  @doc """
  Performs validation on sign up attrs account.
  """
  def changeset(sign_up, attrs) do
    sign_up
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/@/)
    |> update_change(:email, &String.downcase/1)
  end
end
