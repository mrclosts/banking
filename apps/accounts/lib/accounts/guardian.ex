defmodule Accounts.Guardian do
  @moduledoc """
  The `Accounts.Guardian` module uses the Guardian behaviour from the lib guardian
  for managing the app authentication.

  It implements the required functions for the lib to work.
  """

  use Guardian, otp_app: :accounts

  alias Accounts.Account

  def subject_for_token(%Account{} = account, _claims) do
    {:ok, account.email}
  end

  def resource_from_claims(%{"sub" => email}) do
    case Accounts.get_account_by_email(email) do
      nil ->
        {:error, :resource_not_found}

      account ->
        {:ok, account}
    end
  end
end
