defmodule Accounts do
  @moduledoc """
  The module `Accounts` manages the accounts in the application.

  It's responsible for dealing with account creation, authentication and
  fetching and provides the Public API to the other applications of the umbrella
  app. For those goals, it provides three functions: `create_account/1`,
  `authenticate/1` and `get_account_by_email/1`.

  It's composed by the following modules: `Accounts.Account`, `Accounts.Guardian`,
  `Accounts.Profile`, `Accounts.SignIn`, and `Accounts.SignUp`.

  The account's creation flow will first pass through the `Accounts.SignUp` module,
  which will validate the external attributes sent by the `ApiWeb.Accounts.RegistrationsController`
  module. After that, the `Accounts.SignUp` fields will be used to persist the tables
  from both `Accounts.Account` and `Accounts.Profile` schemas. It uses `Ecto.Multi`
  for this process to save all data in one database transaction. This is the ONLY
  way to create a new account into the app.

  The account's authentication flow will first pass through the `Accounts.SignIn` module,
  which will validate the external attributes sent by the `ApiWeb.Accounts.SessionsController`
  module. After that, the `Accounts.SignIn` fields will be sent to `Accounts.Guardian` for
  generating a new authentication token to the client.
  """

  alias Accounts.{Account, Guardian, SignIn}
  alias DataVault.Repo
  alias Ecto.Changeset

  @doc """
  Creates a new account into the application from the given external attributes.

  Returns `{:ok, %Accounts.Account{}, %Accounts.Profile{}}`.

  ## Parameters

    - attributes: Map containing the attributes for a new account.

  """
  def create_account(attributes \\ %{}) do
    with create_account_operations <- Account.create_operations(attributes),
         {:ok, %{account: account, profile: profile}} <-
           Repo.transaction(create_account_operations) do
      {:ok, account, profile}
    else
      {:error, _, failed_value, _} ->
        {:error, failed_value}
    end
  end

  @doc """
  Authenticates an account into the application. It generates a new token to
  the account if all things go well.

  ## Parameters

    - attributes: Map containing the attributes for authenticating the account.

  """
  def authenticate(attributes \\ %{}) do
    changeset = SignIn.changeset(%SignIn{}, attributes)
    sign_in = Changeset.apply_changes(changeset)

    with %Changeset{valid?: true} <- changeset,
         %Account{} = account <- get_account_by_email(sign_in.email),
         {:ok, _} <- Account.check_password(account, sign_in.password),
         {:ok, token, _} <- Guardian.encode_and_sign(account, %{}, token_type: :access) do
      {:ok, token, account}
    else
      %Changeset{valid?: false} = changeset ->
        {:error, changeset}

      _ ->
        {:error, :unauthorized}
    end
  end

  @doc """
  Find an account by email.

  ## Parameters

    - Email: String with the given email to be found.

  """
  def get_account_by_email(email) when not is_nil(email) do
    Repo.get_by(Account, email: email)
  end

  def get_account_by_email(_) do
    {:error, :invalid_email}
  end
end
